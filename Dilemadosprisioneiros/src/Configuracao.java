

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Configuracao {

    private int tamanhoPopulacao;
    private double probabilidadeMutacao;
    private boolean representacao;
    private double probabilidadeCruzamento;
    private int tamanhoIndividuo ;
    private double variancia;
    private double tamanhoSelecao;
    private int condicaoBonus ;
    private double taxaBonus;
    private List<Double> tamanhoNTeste = new ArrayList<>(); //nTeste == 1, 10% e 30% ;
    private Map<String,Double> valoresDeCasos = new HashMap<>();
    private String tipoAvaliacao ;
    private int tamanhoRing;
    private double taxaClassificaoCD;

    public int getTamanhoPopulacao() {
        return tamanhoPopulacao;
    }

    public void setTamanhoPopulacao(int tamanhoPopulacao) {
        this.tamanhoPopulacao = tamanhoPopulacao;
    }

    public double getProbabilidadeMutacao() {
        return probabilidadeMutacao;
    }

    public void setProbabilidadeMutacao(double probabilidadeMutacao) {
        this.probabilidadeMutacao = probabilidadeMutacao;
    }

    public boolean isRepresentacao() {
        return representacao;
    }

    public void setRepresentacao(boolean representacao) {
        this.representacao = representacao;
    }

    public double getProbabilidadeCruzamento() {
        return probabilidadeCruzamento;
    }

    public void setProbabilidadeCruzamento(double probabilidadeCruzamento) {
        this.probabilidadeCruzamento = probabilidadeCruzamento;
    }

    public int getTamanhoIndividuo() {
        return tamanhoIndividuo;
    }

    public void setTamanhoIndividuo(int tamanhoIndividuo) {
        this.tamanhoIndividuo = tamanhoIndividuo;
    }

    public double getVariancia() {
        return variancia;
    }

    public void setVariancia(double variancia) {
        this.variancia = variancia;
    }

    public double getTamanhoSelecao() {
        return tamanhoSelecao;
    }

    public void setTamanhoSelecao(double tamanhoSelecao) {
        this.tamanhoSelecao = tamanhoSelecao;
    }

    public int getCondicaoBonus() {
        return condicaoBonus;
    }

    public void setCondicaoBonus(int condicaoBonus) {
        this.condicaoBonus = condicaoBonus;
    }

    public double getTaxaBonus() {
        return taxaBonus;
    }

    public void setTaxaBonus(double taxaBonus) {
        this.taxaBonus = taxaBonus;
    }

    public List<Double> getTamanhoNTeste() {
        return tamanhoNTeste;
    }

    public void setTamanhoNTeste(List<Double> tamanhoNTeste) {
        this.tamanhoNTeste = tamanhoNTeste;
    }

    public Map<String, Double> getValoresDeCasos() {
        return valoresDeCasos;
    }

    public void setValoresDeCasos(Map<String, Double> valoresDeCasos) {
        this.valoresDeCasos = valoresDeCasos;
    }
    
    public void addValorDeCaso(String nomeCaso,double valor){
        this.valoresDeCasos.put(nomeCaso, valor);
    }
    
    public void addNTeste(double valor){
        this.tamanhoNTeste.add(valor);
    }

    public String getTipoAvaliacao() {
        return tipoAvaliacao;
    }

    public void setTipoAvaliacao(String tipoAvaliacao) {
        this.tipoAvaliacao = tipoAvaliacao;
    }

    public int getTamanhoRing() {
        return tamanhoRing;
    }

    public void setTamanhoRing(int tamanhoRing) {
        this.tamanhoRing = tamanhoRing;
    }

    public double getTaxaClassificaoCD() {
        return taxaClassificaoCD;
    }

    public void setTaxaClassificaoCD(double taxaClassificaoCD) {
        this.taxaClassificaoCD = taxaClassificaoCD;
    }

    
   

    

}
