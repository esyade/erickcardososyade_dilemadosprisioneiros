
import java.util.ArrayList;
import java.util.List;

public class Avaliador {

    private Individuo melhor;

    public Individuo getMelhor() {
        return melhor;
    }

    public void setMelhor(Individuo melhor) {
        this.melhor = melhor;
    }

    public Populacao avaliarPopulacao(Populacao populacao, double TamanhoNTeste, Configuracao configuracao,int indiceTipoFitness) {
        Populacao avaliada = new Populacao();
        for (int w = 0; w < populacao.getTamanho(); w++) {
            Individuo avaliado = populacao.getIndividuoPopulacao(w);
            for (int j = 0; j < TamanhoNTeste; j++) { //cada nTeste tem sua porcentagem
                Individuo parceiro = populacao.getIndividuoPopulacao(Gerador.gerarValorInteiro(populacao.getTamanho()-1));
                avaliado = this.avaliarIndividuo(avaliado, parceiro, configuracao,indiceTipoFitness);
            }
            avaliado = this.aplicaBonusFitness(avaliado, configuracao, indiceTipoFitness);
            avaliada.addIndividuo(avaliado);
        }
        return avaliada;
    }

    public Individuo avaliarIndividuo(Individuo avaliado, Individuo parceiro, Configuracao configuracao, int indiceTipoFitness) {
        Individuo individuoAvaliado;
        if(indiceTipoFitness == 0){
            individuoAvaliado = this.calculaFitnessIndividual(avaliado, parceiro, configuracao,indiceTipoFitness);
        }else{
            individuoAvaliado = this.calculaFitnessIndividuoEmGrupo(avaliado, parceiro, configuracao,indiceTipoFitness);
        }
        return individuoAvaliado;
    }

    public Individuo calculaFitnessIndividual(Individuo individuoAvaliado, Individuo individuoParceiro, Configuracao configuracao, int indiceTipoFitness) {
        double fitness = 0;
        double contadorFitness = 0;
        for (int i = 0; i < individuoAvaliado.getCromossomo().getTamanho(); i++) {
            if (individuoAvaliado.getCromossomo().getGene(i) >= configuracao.getTaxaClassificaoCD() && individuoParceiro.getCromossomo().getGene(i) < configuracao.getTaxaClassificaoCD()) {
                //delatou o parceiro...dc
                contadorFitness += individuoAvaliado.getCromossomo().getGene(i) * configuracao.getValoresDeCasos().get("dc_individuo");
            } else if (individuoAvaliado.getCromossomo().getGene(i) < configuracao.getTaxaClassificaoCD() && individuoParceiro.getCromossomo().getGene(i) < configuracao.getTaxaClassificaoCD()) {
                //os dois cooperam...cc
                contadorFitness += individuoAvaliado.getCromossomo().getGene(i) * configuracao.getValoresDeCasos().get("cc_individuo");
            } else if (individuoAvaliado.getCromossomo().getGene(i) >= configuracao.getTaxaClassificaoCD() && individuoParceiro.getCromossomo().getGene(i) >= configuracao.getTaxaClassificaoCD()) {
                //os dois confessam..dd
                contadorFitness += individuoAvaliado.getCromossomo().getGene(i) * configuracao.getValoresDeCasos().get("dd_individuo");
            } else if (individuoAvaliado.getCromossomo().getGene(i) < configuracao.getTaxaClassificaoCD() && individuoParceiro.getCromossomo().getGene(i) >= configuracao.getTaxaClassificaoCD()) {
                //o individuo coopera e o parceiro o delata..cd
                contadorFitness += individuoAvaliado.getCromossomo().getGene(i) * configuracao.getValoresDeCasos().get("cd_individuo");
            }
        }
        fitness = Gerador.formataCasasDecimais(Gerador.normalizaValorFitness(contadorFitness));
        individuoAvaliado.getFitness()[indiceTipoFitness] = fitness;
        return individuoAvaliado;
    }

    public Individuo calculaFitnessIndividuoEmGrupo(Individuo individuoAvaliado, Individuo individuoParceiro, Configuracao configuracao,int indiceTipoFitness) {
        double fitness = 0;
        double contadorFitness = 0;
        for (int i = 0; i < individuoAvaliado.getCromossomo().getTamanho(); i++) {
            if (individuoAvaliado.getCromossomo().getGene(i) < configuracao.getTaxaClassificaoCD() && individuoParceiro.getCromossomo().getGene(i) < configuracao.getTaxaClassificaoCD()) {
                //os dois cooperam...cc
                contadorFitness += individuoAvaliado.getCromossomo().getGene(i) * configuracao.getValoresDeCasos().get("cc_grupo");
            } else if (individuoAvaliado.getCromossomo().getGene(i) < configuracao.getTaxaClassificaoCD() && individuoParceiro.getCromossomo().getGene(i) >= configuracao.getTaxaClassificaoCD()) {
                //individuo coopera e o outro o delata...cd
                contadorFitness += individuoAvaliado.getCromossomo().getGene(i) * configuracao.getValoresDeCasos().get("cd_grupo");
            } else if (individuoAvaliado.getCromossomo().getGene(i) >= configuracao.getTaxaClassificaoCD() && individuoParceiro.getCromossomo().getGene(i) < configuracao.getTaxaClassificaoCD()) {
                //individuo delata e o outro coopera..dc
                contadorFitness += individuoAvaliado.getCromossomo().getGene(i) * configuracao.getValoresDeCasos().get("dc_grupo");
            } else if (individuoAvaliado.getCromossomo().getGene(i) >= configuracao.getTaxaClassificaoCD() && individuoParceiro.getCromossomo().getGene(i) >= configuracao.getTaxaClassificaoCD()) {
                //os dois se delatam..dd
                contadorFitness += individuoAvaliado.getCromossomo().getGene(i) * configuracao.getValoresDeCasos().get("dd_grupo");
            }
        }
        fitness = Gerador.formataCasasDecimais(Gerador.normalizaValorFitness(contadorFitness));
        individuoAvaliado.getFitness()[indiceTipoFitness] = fitness;
        return individuoAvaliado;
    }

    public void melhorIndividuoPorInteracao(Populacao populacao) {
        for(Individuo individuo : populacao.getPopulacao()) {
            if (individuo.getNivelCooperacaoComParceiro() > this.melhor.getNivelCooperacaoComParceiro()) {
                this.setMelhor(individuo);
            }
        }
    }

    public Individuo aplicaBonusFitness(Individuo individuoAvaliado, Configuracao configuracao,int indiceTipoFitess) {
        int contadorTaxaCooperacao = 0;
        int qtdeCooperacoes = 0;
        int qtdeDelacoes =0;
        double fitness = individuoAvaliado.getFitness()[indiceTipoFitess];
        for (int i = 0; i < individuoAvaliado.getCromossomo().getTamanho(); i++) {
            if (individuoAvaliado.getCromossomo().getGene(i) < configuracao.getTaxaClassificaoCD()) {
                contadorTaxaCooperacao += 1;
                qtdeCooperacoes +=1;
                individuoAvaliado.somaNivelCooperacaoComParceiro(1.0);
                if ((contadorTaxaCooperacao % configuracao.getCondicaoBonus()) == 0) {
                    fitness += individuoAvaliado.getCromossomo().getGene(i) * configuracao.getTaxaBonus();
                }
            } else {
                individuoAvaliado.somaNivelDelacaoDoParceiro(1);
                contadorTaxaCooperacao = 0;
                qtdeDelacoes += 1;
            }
        }
        individuoAvaliado.getFitness()[indiceTipoFitess] = Gerador.formataCasasDecimais(fitness) ;
        individuoAvaliado.setNivelCooperacaoComParceiro(qtdeCooperacoes);
        individuoAvaliado.setNivelDelacaoDoParceiro(qtdeDelacoes);
        return individuoAvaliado;
    }

}
