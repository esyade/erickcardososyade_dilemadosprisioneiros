

import java.util.ArrayList;
import java.util.List;

public class Cromossomo {

    private int tamanho;
    private List<Double> dna = new ArrayList<>();

    public Cromossomo(List<Double> dna, int size) {
        // TODO Configura o cromossomo
        this.dna = dna;
        this.tamanho = size;
    }

    public int getTamanho() {
        // TODO retorna o tamanho do cromossomo
        return this.tamanho;
    }

    public void setDna(List<Double> dna) {
        // TODO insere uma mol�cula de dna
        this.dna = dna;
    }

    public List<Double> getDna() {
        // TODO retorna a molecula de dna
        return this.dna;
    }

    public void addGene(double valorGene) {
        // TODO insere um gene na mol�cula de dna
        this.dna.add(valorGene);
        this.calculaTamanhoCromossomo();
    }

    public Double getGene(int locusGenico) {
        // TODO retorna um gene da mol�cula de dna
        return this.dna.get(locusGenico);
    }

    public void setGene(int posicao, double gene) {
        this.dna.set(posicao, gene);
    }
    
    public String toStringDna(){
        String plot = "\n.................................................................\n" ;
        for(double valor : this.dna){
            plot += valor + "|";
        }
            plot += "\n";
        return plot.replace(".",",") ;
    }

    private void calculaTamanhoCromossomo() {
        this.tamanho++ ;
    }

}
