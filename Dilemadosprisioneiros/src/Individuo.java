
import java.util.ArrayList;
import java.util.List;



public class Individuo {

    private Cromossomo cromossomo;
    private boolean escolhidoCrossOver;
    private boolean escolhidoMutacao;
    private double nivelCooperacaoComParceiro ;
    private double nivelDelacaoDoParceiro;
    private double fitnessIndividual;
    private double fitnessEmGrupo;
    private double[] fitness = {this.fitnessIndividual,this.fitnessEmGrupo};

    public Individuo(Cromossomo cromossomo) {
        this.cromossomo = cromossomo;
    }

    public Cromossomo getCromossomo() {
        return cromossomo;
    }

    public void setCromossomo(Cromossomo cromossomo) {
        this.cromossomo = cromossomo;
    }

    public boolean isEscolhidoCrossOver() {
        return escolhidoCrossOver;
    }

    public void setEscolhidoCrossOver(boolean escolhidoCrossOver) {
        this.escolhidoCrossOver = escolhidoCrossOver;
    }

    public boolean isEscolhidoMutacao() {
        return escolhidoMutacao;
    }

    public void setEscolhidoMutacao(boolean escolhidoMutacao) {
        this.escolhidoMutacao = escolhidoMutacao;
    }

    public double getNivelCooperacaoComParceiro() {
        return nivelCooperacaoComParceiro;
    }

    public void setNivelCooperacaoComParceiro(double nivelCooperacaoComParceiro) {
        this.nivelCooperacaoComParceiro = nivelCooperacaoComParceiro;
    }

    public double getNivelDelacaoDoParceiro() {
        return nivelDelacaoDoParceiro;
    }

    public void setNivelDelacaoDoParceiro(double nivelDelacaoDoParceiro) {
        this.nivelDelacaoDoParceiro = nivelDelacaoDoParceiro;
    }

    public double getFitnessIndividual() {
        return fitness[0];
    }

    public void setFitnessIndividual(double fitnessIndividual) {
        this.fitness[0] = fitnessIndividual;
    }

    public double getFitnessEmGrupo() {
        return fitness[1];
    }

    public void setFitnessEmGrupo(double fitnessEmGrupo) {
        this.fitness[1] = fitnessEmGrupo;
    }

    public void somaNivelDelacaoDoParceiro(double valor ) {
        double novoValor = this.nivelDelacaoDoParceiro + valor ;
        this.nivelDelacaoDoParceiro = novoValor ;
    }

    void somaNivelCooperacaoComParceiro(Double valor) {
        double novoValor = this.nivelCooperacaoComParceiro + valor ;
        this.nivelDelacaoDoParceiro = novoValor ;
    }

    public double[] getFitness() {
        return fitness;
    }

    
    
    
    
    
    

}