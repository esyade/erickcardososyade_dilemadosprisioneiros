
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Ag {

    private Configuracao configuracao;
    private Avaliador avaliador;
    private Populacao populacao;

    public void addConfiguracao(Configuracao config) {
        this.configuracao = config;
    }

    public Configuracao getConfiguracao() {
        return this.configuracao;
    }

    public Populacao ordenaPopulacao(Populacao populacaoAvaliada,int indiceTipoFitness) {
        // TODO Auto-generated method stub
        Collections.sort(populacaoAvaliada.getPopulacao(), (Object o1, Object o2) -> {
            Individuo p1 = (Individuo) o1;
            Individuo p2 = (Individuo) o2;
            return p1.getFitness()[indiceTipoFitness]> p2.getFitness()[indiceTipoFitness] ? -1 : (p1.getFitness()[indiceTipoFitness] < p2.getFitness()[indiceTipoFitness] ? +1 : 0);
        });
        return populacaoAvaliada;
    }
    
    private List<Individuo> montaRing(Populacao populacaoAvaliada) {
        double quantidadeCasais = this.configuracao.getTamanhoRing();
        int numeroIndice;
        List<Individuo> listaCompetidores = new ArrayList<>();
        for (int i = 0; i < quantidadeCasais; i++) {
            numeroIndice = Gerador.gerarValorInteiro(populacaoAvaliada.getTamanho() - 1);
            listaCompetidores.add(populacaoAvaliada.getIndividuoPopulacao(numeroIndice));
        }
        return listaCompetidores;
    }

    private Individuo extraiMelhorIndividuoRing(List<Individuo> ring,int indiceTipoFitness) {
        Individuo melhorDoRing = null;
        double melhorFitness = 0;
        for (int i = 0; i < ring.size(); i++) {
                if (ring.get(i).getFitness()[indiceTipoFitness] != melhorFitness) {
                    melhorDoRing = ring.get(i);
                    melhorFitness = melhorDoRing.getFitness()[indiceTipoFitness];
                }
        }
        return melhorDoRing;
    }

    public Populacao selecaoPorTorneio(Populacao populacaoAvaliada, int indiceTipoFitness) {
        List<Individuo> ring;
        Populacao populacaoParaCrossOver = new Populacao();
        Individuo melhorIndividuoDoRing;
        for (int i = 0; i < populacaoAvaliada.getTamanho(); i++) {
            ring = this.montaRing(populacaoAvaliada);
            melhorIndividuoDoRing = this.extraiMelhorIndividuoRing(ring,indiceTipoFitness);
            populacaoParaCrossOver.addIndividuo(melhorIndividuoDoRing);
        }

        return populacaoParaCrossOver;
    }

    public List<Individuo> recombinacaoGenica(Individuo individuoX, Individuo individuoY) {
        List<Double> dnaX = new ArrayList<>() , dnaY = new ArrayList<>();
        Individuo filhoX, filhoY;
        Cromossomo cromossomoX = new Cromossomo(dnaX, dnaX.size()), cromossomoY = new Cromossomo(dnaY, dnaY.size());
        double numeroAleatorio = Gerador.gerarValorReal();
        if (numeroAleatorio < this.configuracao.getProbabilidadeCruzamento()) {
            for (int iRecomb = 0; iRecomb < individuoX.getCromossomo().getTamanho(); iRecomb++) {
                double geneFilhoX = Gerador.formataCasasDecimais(individuoX.getCromossomo().getGene(iRecomb) + ( individuoY.getCromossomo().getGene(iRecomb) - individuoX.getCromossomo().getGene(iRecomb) ) * Gerador.gerarValorPorUniforme());
                double geneFilhoY = Gerador.formataCasasDecimais(individuoY.getCromossomo().getGene(iRecomb) + ( individuoX.getCromossomo().getGene(iRecomb) - individuoY.getCromossomo().getGene(iRecomb) ) * Gerador.gerarValorPorUniforme());
                cromossomoX.addGene(geneFilhoX);
                cromossomoY.addGene(geneFilhoY);
            }
        filhoX = new Individuo(cromossomoX) ;filhoY = new Individuo(cromossomoY);
        filhoX.setEscolhidoCrossOver(true); filhoY.setEscolhidoCrossOver(true); 
        }else{
            filhoX = individuoX ; filhoY = individuoY;
        }
        List<Individuo> duplaFilhos = new ArrayList<>();
        duplaFilhos.add(filhoX);
        duplaFilhos.add(filhoY);
        return duplaFilhos;
    }

    public Populacao crossover(Populacao populacaoOrdenada) {
        Populacao geracao = new Populacao();
        double numeroAleatorio = 0.0;
        int i = 0;
        do {
            List<Individuo> dpFilhos;
            dpFilhos = this.recombinacaoGenica(populacaoOrdenada.getIndividuoPopulacao(i), populacaoOrdenada.getIndividuoPopulacao(i + 1));
            geracao.addIndividuo(dpFilhos.get(0));
            geracao.addIndividuo(dpFilhos.get(1));
            i += 2;
        } while (geracao.getTamanho() != this.configuracao.getTamanhoPopulacao());

        return geracao;
    }
    
    private Individuo mutaGeneIndividuo(Individuo individuoPopulacao) {
        List<Double> novoDna = new ArrayList<>();
        boolean mutacao = false;
        for (int i = 0; i < individuoPopulacao.getCromossomo().getTamanho(); i++) {
            double k = Gerador.gerarValorReal();
            double novoGene;
            if (k < this.configuracao.getProbabilidadeMutacao()) {
                novoGene = Gerador.formataCasasDecimais(Gerador.gerarValorGaussiana(individuoPopulacao.getCromossomo().getGene(i), this.configuracao.getVariancia()));
                individuoPopulacao.getCromossomo().setGene(i, novoGene);
                mutacao = true;
            }else{
                novoGene = individuoPopulacao.getCromossomo().getGene(i);
            }
            novoDna.add(novoGene) ;
        }
        Cromossomo novoCromossomo = new Cromossomo(novoDna,novoDna.size());
        individuoPopulacao.setCromossomo(novoCromossomo);
        individuoPopulacao.setEscolhidoMutacao(mutacao);
        return individuoPopulacao;
    }

    public Populacao mutacao(Populacao novaGeracao) {
        Populacao populacaoMutada = new Populacao();
        for (int i = 0; i < novaGeracao.getTamanho(); i++) {
            Individuo individuoComGenesMutados = this.mutaGeneIndividuo(novaGeracao.getIndividuoPopulacao(i));
            populacaoMutada.addIndividuo(individuoComGenesMutados);
        }
        return populacaoMutada;
    }

//
//    public double calculaPorIteracao(Populacao populacaoMelhores) {
//         double soma = 0.0;
//         double media;
//            for(int j = 0 ; j < populacaoMelhores.getTamanho(); j++){
//                soma += populacaoMelhores.getIndividuoPopulacao(j).getFitnessIndividual() ;
//            }
//         media = (soma / 5) ;
//         return media;
//    }

    
    
    

   

}
