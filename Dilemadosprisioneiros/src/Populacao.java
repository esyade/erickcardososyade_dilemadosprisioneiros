

import java.util.ArrayList;
import java.util.List;

public class Populacao {

    private int tamanho;
    private List<Individuo> listaIndividuos;
    
    public Populacao(){
        this.listaIndividuos = new ArrayList<>();
    }

    public void addIndividuo(Individuo individuo) {
        // TODO adiciona um indiv�duo � popula��o
        this.listaIndividuos.add(individuo);
        this.calculaTamanhoPopulacao();
    }

    public int getTamanho() {
        // TODO retorna o tamanho da populacao...
        return this.tamanho;
    }

    public void setTamanho(int quantidadeIndividuos) {
        // TODO insere o tamanho de uma populacao
        this.tamanho = quantidadeIndividuos;
    }

    private void calculaTamanhoPopulacao() {
        // TODO calcula o size da lista de indiv�duos
        this.tamanho = this.listaIndividuos.size();

    }

    public void setPopulacao(List<Individuo> listaIndividuos2) {
        // TODO adiciona uma lista de indiv�duos
            this.listaIndividuos = listaIndividuos2;
            this.calculaTamanhoPopulacao();
    }

    public List<Individuo> getPopulacao() {
        // TODO adiciona uma lista de indiv�duos
        return this.listaIndividuos;
    }

    public Individuo getIndividuoPopulacao(int posicaoIndividuo) {
        // TODO retorna um indiv�duo da lista
        return this.listaIndividuos.get(posicaoIndividuo);
    }

    public void setIndividuoPopulacao(int posicao, Individuo individuo) {
        // TODO altera o valor de um indiv�duo da lista
        Individuo novoIndividuo = individuo ;
        this.listaIndividuos.set(posicao, novoIndividuo);
    }

    public void exibeIndividuos(String titulo) {
        System.out.println(titulo);
        for (Individuo individuo : this.listaIndividuos) {
        System.out.println("------------------------- \n");
            System.out.println("dna do Indivíduo: "+individuo.getCromossomo().toStringDna()
                    + "\n Fitness Individual: " + individuo.getFitnessIndividual() 
                    + "\n Fitness em Grupo: " + individuo.getFitnessEmGrupo()
                    + "\n Escolhido para crossover: " + individuo.isEscolhidoCrossOver() 
                    + "\n Escolhido para mutação: " + individuo.isEscolhidoMutacao()
                    + "\n cooperações com o parceiro: " + individuo.getNivelCooperacaoComParceiro()
                    + "\n delações do o parceiro: " + individuo.getNivelCooperacaoComParceiro()
            );
        }
        System.out.println("--------------------------- \n");

    }

}
