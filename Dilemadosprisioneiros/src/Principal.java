//
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import org.jfree.data.category.DefaultCategoryDataset;
//
///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
///**
// *
// * @author ERICK-PC
// */
//public class Principal {
//
//    public static void main(String[] args) {
//        //errado
//        Ag ag = new Ag();
//        Grafico grafico = new Grafico();
//        DefaultCategoryDataset ds = new DefaultCategoryDataset();
//        Avaliador avaliador = new Avaliador();
//        Configuracao config;
//        config = new Configuracao(100, 1.0, true, 1.0, 5, 0.45, 100*0.06);
//        ag.addConfiguracao(config);
//        List<Double> dnaSolucao = Gerador.gerarListaDeValoresReais(config.getTamanhoIndividuo());
//        Cromossomo cromossomoSolucao = new Cromossomo(dnaSolucao, dnaSolucao.size());
//        Individuo individuoSolucao = new Individuo(cromossomoSolucao);
//        avaliador.setSolucao(dnaSolucao);
//        List<Double> listaDeSomas = new ArrayList<>();
//        Populacao melhores = new Populacao();
//        System.out.println("Individuo Solução :" + individuoSolucao.getCromossomo().toStringDna());
//        int condicaoParada = 0;
//        Individuo melhor = new Individuo(Gerador.gerarCromossmoAleatorio(0));
//        melhor.setFitness(0);
//        int iteracao = 0;
//        avaliador.setMelhor(melhor);
//        List<List<Double>> fitnessExecucao = new ArrayList<>();
//        List<Double> fitnessIteracao = new ArrayList<>();
//        for (int w = 0; w < 5; w++) {
//            Populacao populacao = Gerador.gerarPopulacaoAleatoriaIndividuos(config.getTamanhoPopulacao(), config.getTamanhoIndividuo());
//            for (int i = 0; i < 500; i++) {
//                Populacao populacaoAvaliada = avaliador.avaliarPopulacao(populacao);
//                    Populacao populacaoOrdenada = ag.ordenaPopulacao(populacaoAvaliada);
//                    Populacao  populacaoComEscolhidos = ag.selecaoPorTorneio(populacaoOrdenada);
//                    Populacao populacaoCruzada = ag.crossover(populacaoComEscolhidos);
//                    Populacao populacaoMutada = ag.mutacao(populacaoCruzada);
//                    Populacao populacaoMutadaAvaliada = avaliador.avaliarPopulacao(populacaoMutada);
//                    avaliador.melhorIndividuoPorInteracao(populacaoMutadaAvaliada) ;  
//                    populacao = populacaoMutadaAvaliada ;
//                    melhor = avaliador.getMelhor();
//                    fitnessIteracao.add((double)melhor.getNivelCooperacaoComParceiro());
//            }
//            melhores.exibeIndividuos("Execução -> " + w + " Ok");
//            fitnessExecucao.add(fitnessIteracao);
//        }
//        double media = 0;
//        for (int i = 0; i < fitnessIteracao.size(); i++) {
//            double soma = 0;
//            for (int j = 0; j < fitnessExecucao.size(); j++) {
//                soma = fitnessExecucao.get(j).get(i);
//                media = soma ;
//            }
//            try {
//                ds = grafico.configuracaoDataSerGrafico(ds, media, "T" + config.getTamanhoIndividuo() + "N" + config.getTamanhoPopulacao(), i);
//            } catch (IOException ex) {
//                Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
//
//        try {
//            grafico.mostraGrafico(grafico.painelGrafico(ds));
//        } catch (IOException ex) {
//            System.out.println(ex.getMessage());
//        }
//    }
//}
