

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

public class TesteAvaliador {

    @Test
    public void testCalculaFitnessIndividuo() {
        Configuracao configuracao = new Configuracao();
        configuracao.setTamanhoPopulacao(50);
        configuracao.setTamanhoIndividuo(30);
        configuracao.setProbabilidadeCruzamento(0.6);
        configuracao.setProbabilidadeMutacao(0.3);
        configuracao.setTamanhoSelecao(5);
        configuracao.setVariancia(0.08);
        configuracao.setTaxaBonus(0.05);
        configuracao.setCondicaoBonus(4);
        configuracao.addNTeste(1.0);
        configuracao.addNTeste(0.10 * configuracao.getTamanhoPopulacao());
        configuracao.addNTeste(0.50 * configuracao.getTamanhoPopulacao());
        configuracao.addValorDeCaso("dc_individuo",1.0);
        configuracao.addValorDeCaso("cc_individuo",0.90);
        configuracao.addValorDeCaso("dd_individuo",0.50);
        configuracao.addValorDeCaso("cd_individuo",0.01);
        configuracao.addValorDeCaso("cc_grupo",1.0);
        configuracao.addValorDeCaso("cd_grupo",0.50);
        configuracao.addValorDeCaso("dc_grupo",0.50);
        configuracao.addValorDeCaso("dd_grupo",0.01);
        configuracao.setTipoAvaliacao("indvidual");
        configuracao.setTaxaClassificaoCD(0.5);
        List<Double> dna1 = new ArrayList<>();
        dna1.add(0.6);
        dna1.add(0.6);
        dna1.add(0.6);
        dna1.add(0.6);
        dna1.add(0.6);
        dna1.add(0.6);
        dna1.add(0.6);
        dna1.add(0.6);
        dna1.add(0.6);
        dna1.add(0.6);
        dna1.add(0.6);
        dna1.add(0.6);

        Cromossomo cromossomo1 = new Cromossomo(dna1, dna1.size());
        Individuo avaliado = new Individuo(cromossomo1);

        List<Double> dna2 = new ArrayList<>();
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);

        Cromossomo cromossomo2 = new Cromossomo(dna2, dna2.size());
        Individuo parceiro = new Individuo(cromossomo2);
        
        Avaliador avaliador = new Avaliador();
        assertEquals(0.72, avaliador.calculaFitnessIndividual(avaliado,parceiro,configuracao,0).getFitnessIndividual(),0);
    }
    
    @Test
    public void testCalculaFitnessIndividuoEmGrupo() {
        Configuracao configuracao = new Configuracao();
        configuracao.setTamanhoPopulacao(50);
        configuracao.setTamanhoIndividuo(30);
        configuracao.setProbabilidadeCruzamento(0.6);
        configuracao.setProbabilidadeMutacao(0.3);
        configuracao.setTamanhoSelecao(5);
        configuracao.setVariancia(0.08);
        configuracao.setTaxaBonus(0.05);
        configuracao.setCondicaoBonus(4);
        configuracao.addNTeste(1.0);
        configuracao.addNTeste(0.10 * configuracao.getTamanhoPopulacao());
        configuracao.addNTeste(0.50 * configuracao.getTamanhoPopulacao());
        configuracao.addValorDeCaso("dc_individuo",1.0);
        configuracao.addValorDeCaso("cc_individuo",0.90);
        configuracao.addValorDeCaso("dd_individuo",0.50);
        configuracao.addValorDeCaso("cd_individuo",0.01);
        configuracao.addValorDeCaso("cc_grupo",1.0);
        configuracao.addValorDeCaso("cd_grupo",0.50);
        configuracao.addValorDeCaso("dc_grupo",0.50);
        configuracao.addValorDeCaso("dd_grupo",0.01);
        configuracao.setTipoAvaliacao("indvidual");
        configuracao.setTaxaClassificaoCD(0.5);
        
        List<Double> dna1 = new ArrayList<>();
        dna1.add(0.2);
        dna1.add(0.2);
        dna1.add(0.2);
        dna1.add(0.2);
        dna1.add(0.2);
        dna1.add(0.2);
        dna1.add(0.2);
        dna1.add(0.2);
        dna1.add(0.2);
        dna1.add(0.2);
        dna1.add(0.2);
        dna1.add(0.2);

        List<Double> dna2 = new ArrayList<>();
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);

        Cromossomo cromossomo1 = new Cromossomo(dna1, dna1.size());
        Individuo avaliado = new Individuo(cromossomo1);
        
        Cromossomo cromossomo2 = new Cromossomo(dna2, dna2.size());
        Individuo parceiro = new Individuo(cromossomo2);
        
        Avaliador avaliador = new Avaliador();
        assertEquals(0.24, avaliador.calculaFitnessIndividuoEmGrupo(avaliado,parceiro,configuracao,1).getFitnessEmGrupo(),0);
    }
    
    @Test
    public void testAvaliaPopulacaoParaFitnessIndividual() {
        Configuracao configuracao = new Configuracao();
        Populacao populacao = new Populacao();
        configuracao.setTamanhoPopulacao(50);
        configuracao.setTamanhoIndividuo(30);
        configuracao.setProbabilidadeCruzamento(0.6);
        configuracao.setProbabilidadeMutacao(0.3);
        configuracao.setTamanhoSelecao(5);
        configuracao.setVariancia(0.08);
        configuracao.setTaxaBonus(0.05);
        configuracao.setCondicaoBonus(4);
        configuracao.addNTeste(1.0);
        configuracao.addNTeste(0.10 * configuracao.getTamanhoPopulacao());
        configuracao.addNTeste(0.50 * configuracao.getTamanhoPopulacao());
        configuracao.addValorDeCaso("dc_individuo",1.0);
        configuracao.addValorDeCaso("cc_individuo",0.90);
        configuracao.addValorDeCaso("dd_individuo",0.50);
        configuracao.addValorDeCaso("cd_individuo",0.01);
        configuracao.addValorDeCaso("cc_grupo",1.0);
        configuracao.addValorDeCaso("cd_grupo",0.50);
        configuracao.addValorDeCaso("dc_grupo",0.50);
        configuracao.addValorDeCaso("dd_grupo",0.01);
        configuracao.setTipoAvaliacao("indvidual");
        configuracao.setTaxaClassificaoCD(0.5);
        
        List<Double> dna1 = new ArrayList<>();
        dna1.add(0.2);
        dna1.add(0.2);
        dna1.add(0.2);
        dna1.add(0.2);
        dna1.add(0.2);
        dna1.add(0.2);
        dna1.add(0.2);
        dna1.add(0.2);
        dna1.add(0.2);
        dna1.add(0.2);
        dna1.add(0.2);
        dna1.add(0.2);

        List<Double> dna2 = new ArrayList<>();
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        
        Cromossomo cromossomo1 = new Cromossomo(dna1, dna1.size());
        Individuo avaliado = new Individuo(cromossomo1);
        
        Cromossomo cromossomo2 = new Cromossomo(dna2, dna2.size());
        Individuo parceiro = new Individuo(cromossomo2);
        
        populacao.addIndividuo(avaliado);
        populacao.addIndividuo(parceiro);
        
        Avaliador avaliador = new Avaliador();
        assertEquals(0.22, avaliador.avaliarPopulacao(populacao, configuracao.getTamanhoNTeste().get(0), configuracao, 0).getIndividuoPopulacao(0).getFitnessIndividual(),0);
    }
    
    @Test
    public void testAvaliaPopulacaoParaFitnessGrupo() {
        Configuracao configuracao = new Configuracao();
        Populacao populacao = new Populacao();
        configuracao.setTamanhoPopulacao(50);
        configuracao.setTamanhoIndividuo(30);
        configuracao.setProbabilidadeCruzamento(0.6);
        configuracao.setProbabilidadeMutacao(0.3);
        configuracao.setTamanhoSelecao(5);
        configuracao.setVariancia(0.08);
        configuracao.setTaxaBonus(0.05);
        configuracao.setCondicaoBonus(4);
        configuracao.addNTeste(1.0);
        configuracao.addNTeste(0.10 * configuracao.getTamanhoPopulacao());
        configuracao.addNTeste(0.50 * configuracao.getTamanhoPopulacao());
        configuracao.addValorDeCaso("dc_individuo",1.0);
        configuracao.addValorDeCaso("cc_individuo",0.90);
        configuracao.addValorDeCaso("dd_individuo",0.50);
        configuracao.addValorDeCaso("cd_individuo",0.01);
        configuracao.addValorDeCaso("cc_grupo",1.0);
        configuracao.addValorDeCaso("cd_grupo",0.50);
        configuracao.addValorDeCaso("dc_grupo",0.50);
        configuracao.addValorDeCaso("dd_grupo",0.01);
        configuracao.setTipoAvaliacao("indvidual");
        configuracao.setTaxaClassificaoCD(0.5);
        
        List<Double> dna1 = new ArrayList<>();
        dna1.add(0.2);
        dna1.add(0.2);
        dna1.add(0.2);
        dna1.add(0.2);
        dna1.add(0.2);
        dna1.add(0.2);
        dna1.add(0.2);
        dna1.add(0.2);
        dna1.add(0.2);
        dna1.add(0.2);
        dna1.add(0.2);
        dna1.add(0.2);

        List<Double> dna2 = new ArrayList<>();
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        
        Cromossomo cromossomo1 = new Cromossomo(dna1, dna1.size());
        Individuo avaliado = new Individuo(cromossomo1);
        
        Cromossomo cromossomo2 = new Cromossomo(dna2, dna2.size());
        Individuo parceiro = new Individuo(cromossomo2);
        
        populacao.addIndividuo(avaliado);
        populacao.addIndividuo(parceiro);
        
        Avaliador avaliador = new Avaliador();
        assertEquals(0.24, avaliador.avaliarPopulacao(populacao, configuracao.getTamanhoNTeste().get(0), configuracao, 1).getIndividuoPopulacao(0).getFitnessEmGrupo(),0);
    }
    
    @Test
    public void testAplicacaoBonusFitnessIndividual() {
        Configuracao configuracao = new Configuracao();
        Populacao populacao = new Populacao();
        configuracao.setTamanhoPopulacao(50);
        configuracao.setTamanhoIndividuo(30);
        configuracao.setProbabilidadeCruzamento(0.6);
        configuracao.setProbabilidadeMutacao(0.3);
        configuracao.setTamanhoSelecao(5);
        configuracao.setVariancia(0.08);
        configuracao.setTaxaBonus(0.05);
        configuracao.setCondicaoBonus(4);
        configuracao.addNTeste(1.0);
        configuracao.addNTeste(0.10 * configuracao.getTamanhoPopulacao());
        configuracao.addNTeste(0.50 * configuracao.getTamanhoPopulacao());
        configuracao.addValorDeCaso("dc_individuo",1.0);
        configuracao.addValorDeCaso("cc_individuo",0.90);
        configuracao.addValorDeCaso("dd_individuo",0.50);
        configuracao.addValorDeCaso("cd_individuo",0.01);
        configuracao.addValorDeCaso("cc_grupo",1.0);
        configuracao.addValorDeCaso("cd_grupo",0.50);
        configuracao.addValorDeCaso("dc_grupo",0.50);
        configuracao.addValorDeCaso("dd_grupo",0.01);
        configuracao.setTipoAvaliacao("indvidual");
        configuracao.setTaxaClassificaoCD(0.5);
        
        List<Double> dna1 = new ArrayList<>();
        dna1.add(0.2);
        dna1.add(0.2);
        dna1.add(0.2);
        dna1.add(0.2);
        dna1.add(0.2);
        dna1.add(0.2);
        dna1.add(0.2);
        dna1.add(0.2);
        dna1.add(0.2);
        dna1.add(0.2);
        dna1.add(0.2);
        dna1.add(0.2);

        List<Double> dna2 = new ArrayList<>();
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        
        Cromossomo cromossomo1 = new Cromossomo(dna1, dna1.size());
        Individuo avaliado = new Individuo(cromossomo1);
        
        Cromossomo cromossomo2 = new Cromossomo(dna2, dna2.size());
        Individuo parceiro = new Individuo(cromossomo2);
        
        populacao.addIndividuo(avaliado);
        populacao.addIndividuo(parceiro);
        
        Avaliador avaliador = new Avaliador();
        assertEquals(0.03, avaliador.aplicaBonusFitness(avaliado, configuracao, 0).getFitnessIndividual(),0);
    }
    
    @Test
    public void testAplicacaoBonusFitnessEmgrupo() {
        Configuracao configuracao = new Configuracao();
        Populacao populacao = new Populacao();
        configuracao.setTamanhoPopulacao(50);
        configuracao.setTamanhoIndividuo(30);
        configuracao.setProbabilidadeCruzamento(0.6);
        configuracao.setProbabilidadeMutacao(0.3);
        configuracao.setTamanhoSelecao(5);
        configuracao.setVariancia(0.08);
        configuracao.setTaxaBonus(0.05);
        configuracao.setCondicaoBonus(4);
        configuracao.addNTeste(1.0);
        configuracao.addNTeste(0.10 * configuracao.getTamanhoPopulacao());
        configuracao.addNTeste(0.50 * configuracao.getTamanhoPopulacao());
        configuracao.addValorDeCaso("dc_individuo",1.0);
        configuracao.addValorDeCaso("cc_individuo",0.90);
        configuracao.addValorDeCaso("dd_individuo",0.50);
        configuracao.addValorDeCaso("cd_individuo",0.01);
        configuracao.addValorDeCaso("cc_grupo",1.0);
        configuracao.addValorDeCaso("cd_grupo",0.50);
        configuracao.addValorDeCaso("dc_grupo",0.50);
        configuracao.addValorDeCaso("dd_grupo",0.01);
        configuracao.setTipoAvaliacao("indvidual");
        configuracao.setTaxaClassificaoCD(0.5);
        
        List<Double> dna1 = new ArrayList<>();
        dna1.add(0.2);
        dna1.add(0.2);
        dna1.add(0.2);
        dna1.add(0.2);
        dna1.add(0.2);
        dna1.add(0.2);
        dna1.add(0.2);
        dna1.add(0.2);
        dna1.add(0.2);
        dna1.add(0.2);
        dna1.add(0.2);
        dna1.add(0.2);

        List<Double> dna2 = new ArrayList<>();
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        dna2.add(0.2);
        
        Cromossomo cromossomo1 = new Cromossomo(dna1, dna1.size());
        Individuo avaliado = new Individuo(cromossomo1);
        
        Cromossomo cromossomo2 = new Cromossomo(dna2, dna2.size());
        Individuo parceiro = new Individuo(cromossomo2);
        
        populacao.addIndividuo(avaliado);
        populacao.addIndividuo(parceiro);
        
        Avaliador avaliador = new Avaliador();
        assertEquals(0.03, avaliador.aplicaBonusFitness(avaliado, configuracao, 1).getFitnessEmGrupo(),0);
    }
    
    
    
    
    

}
