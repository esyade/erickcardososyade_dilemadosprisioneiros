import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class TesteIndividuo {

	@Test
	public void testAddCromossomo() {
		List<Double> dna = new ArrayList<>() ;
		dna.add(0, 0.11);
		dna.add(1, 0.41);
		dna.add(2, 0.41);
		dna.add(3, 0.03);
		dna.add(4, 0.51);
		dna.add(5, 0.41);
		dna.add(6, 0.71);
		Cromossomo cromossomo = new Cromossomo(dna,dna.size());
		Individuo individuo = new Individuo(cromossomo);
		assertNotNull(individuo.getCromossomo());
	}

        @Test
	public void testIndividuoAleatorio() {
		List<Double> dna = Gerador.gerarListaDeValores(5);
		
		Cromossomo cromossomo = new Cromossomo(dna,dna.size());
		Individuo individuo = new Individuo(cromossomo);
		assertEquals(5, individuo.getCromossomo().getTamanho(),0);
	}
        
        @Test
	public void testFitnessIndividual() {
		List<Double> dna = Gerador.gerarListaDeValores(5);
		
		Cromossomo cromossomo = new Cromossomo(dna,dna.size());
		Individuo individuo = new Individuo(cromossomo);
                individuo.setFitnessIndividual(0.99);
		assertEquals(0.99, individuo.getFitnessIndividual(),0);
	}
        
        @Test
	public void testFitnessEmGrupo() {
		List<Double> dna = Gerador.gerarListaDeValores(5);
		
		Cromossomo cromossomo = new Cromossomo(dna,dna.size());
		Individuo individuo = new Individuo(cromossomo);
                individuo.setFitnessEmGrupo(0.99);
		assertEquals(0.99, individuo.getFitnessEmGrupo(),0);
	}
        
        
	
	

}
