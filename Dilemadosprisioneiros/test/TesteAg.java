
import java.io.FileNotFoundException;
import java.io.IOException;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Before;
import org.junit.BeforeClass;

import org.junit.Test;

public class TesteAg {
    private static Configuracao configuracao;
    private static Ag ag;
    private static Populacao populacao;
    private static Avaliador avaliador;
    @BeforeClass
    public static void setUpClass() {
        configuracao = new Configuracao();
        Populacao populacao = new Populacao();
        configuracao.setTamanhoPopulacao(50);
        configuracao.setTamanhoIndividuo(30);
        configuracao.setProbabilidadeCruzamento(0.6);
        configuracao.setProbabilidadeMutacao(0.3);
        configuracao.setTamanhoSelecao(5);
        configuracao.setVariancia(0.08);
        configuracao.setTaxaBonus(0.05);
        configuracao.setCondicaoBonus(4);
        configuracao.addNTeste(1.0);
        configuracao.addNTeste(0.10 * configuracao.getTamanhoPopulacao());
        configuracao.addNTeste(0.50 * configuracao.getTamanhoPopulacao());
        configuracao.addValorDeCaso("dc_individuo",1.0);
        configuracao.addValorDeCaso("cc_individuo",0.90);
        configuracao.addValorDeCaso("dd_individuo",0.50);
        configuracao.addValorDeCaso("cd_individuo",0.01);
        configuracao.addValorDeCaso("cc_grupo",1.0);
        configuracao.addValorDeCaso("cd_grupo",0.50);
        configuracao.addValorDeCaso("dc_grupo",0.50);
        configuracao.addValorDeCaso("dd_grupo",0.01);
        configuracao.setTipoAvaliacao("indvidual");
        configuracao.setTaxaClassificaoCD(0.5);
        configuracao.setTamanhoRing(5);
    }
    
    @Before
    public void setUp() {
        ag = new Ag();
        populacao = new Populacao();
        avaliador = new Avaliador();
        ag.addConfiguracao(configuracao);
        
    }
    
    @Test
    public void testAddConfiguracao() {
        assertNotNull(ag.getConfiguracao());
    }

    @Test
    public void testOrdenarPopulacaoFitnessIndividual() {
        populacao = Gerador.gerarPopulacaoAleatoriaIndividuos(configuracao.getTamanhoPopulacao(), configuracao.getTamanhoIndividuo());
        Populacao populacaoAvaliada = avaliador.avaliarPopulacao(populacao, configuracao.getTamanhoNTeste().get(2), configuracao, 0);
        Populacao populacaoOrdenadaFitnessIndividual = ag.ordenaPopulacao(populacaoAvaliada, 0);
        populacaoOrdenadaFitnessIndividual.exibeIndividuos("populacao ordenada pelo fitness indiidual");
    }
    
    @Test
    public void testOrdenarPopulacaoFitnessEmGrupo() {
        populacao = Gerador.gerarPopulacaoAleatoriaIndividuos(configuracao.getTamanhoPopulacao(), configuracao.getTamanhoIndividuo());
        Populacao populacaoAvaliada = avaliador.avaliarPopulacao(populacao, configuracao.getTamanhoNTeste().get(2), configuracao, 1);
        Populacao populacaoOrdenadaFitnessGrupo = ag.ordenaPopulacao(populacaoAvaliada, 1);
        populacaoOrdenadaFitnessGrupo.exibeIndividuos("populacao ordenada pelo fitness em grupo");
    }

    @Test
    public void testSelecaoPorTorneioFitnessIndividual() {
        populacao = Gerador.gerarPopulacaoAleatoriaIndividuos(configuracao.getTamanhoPopulacao(), configuracao.getTamanhoIndividuo());
        Populacao populacaoAvaliada = avaliador.avaliarPopulacao(populacao, configuracao.getTamanhoNTeste().get(2), configuracao, 0);
        Populacao populacaoOrdenadaFitnessIndividual = ag.ordenaPopulacao(populacaoAvaliada, 0);
        assertEquals(50,populacaoOrdenadaFitnessIndividual.getTamanho());
        Populacao populacaoSelecionadaFitnessIndividual = ag.selecaoPorTorneio(populacaoOrdenadaFitnessIndividual, 0);
        assertEquals(50,populacaoSelecionadaFitnessIndividual.getTamanho());
        populacaoSelecionadaFitnessIndividual.exibeIndividuos("populacao selecionada");
    }
    
    @Test
    public void testSelecaoPorTorneioFitnessEmGrupo() {
        populacao = Gerador.gerarPopulacaoAleatoriaIndividuos(configuracao.getTamanhoPopulacao(), configuracao.getTamanhoIndividuo());
        Populacao populacaoAvaliada = avaliador.avaliarPopulacao(populacao, configuracao.getTamanhoNTeste().get(2), configuracao, 1);
        Populacao populacaoOrdenadaFitnessGrupo = ag.ordenaPopulacao(populacaoAvaliada, 1);
        assertEquals(50,populacaoOrdenadaFitnessGrupo.getTamanho());
        Populacao populacaoSelecionadaFitnessGrupo = ag.selecaoPorTorneio(populacaoOrdenadaFitnessGrupo, 1);
        assertEquals(50,populacaoSelecionadaFitnessGrupo.getTamanho());
        populacaoSelecionadaFitnessGrupo.exibeIndividuos("populacao selecionada");
    }
        
     @Test
     public void testCrossOver(){
        populacao = Gerador.gerarPopulacaoAleatoriaIndividuos(configuracao.getTamanhoPopulacao(), configuracao.getTamanhoIndividuo());
        Populacao populacaoAvaliada = avaliador.avaliarPopulacao(populacao, configuracao.getTamanhoNTeste().get(2), configuracao, 0);
        Populacao populacaoOrdenadaFitnessIndividual = ag.ordenaPopulacao(populacaoAvaliada, 0);
        assertEquals(50,populacaoOrdenadaFitnessIndividual.getTamanho());
        Populacao populacaoSelecionadaFitnessIndividual = ag.selecaoPorTorneio(populacaoOrdenadaFitnessIndividual, 0);
        assertEquals(50,populacaoSelecionadaFitnessIndividual.getTamanho());     
        Populacao populacaoCruzada = ag.crossover(populacaoAvaliada);
        assertEquals(50,populacaoCruzada.getTamanho());
        populacaoCruzada.exibeIndividuos("populacao cruzada");
     }

     @Test
     public void testMutacao(){
        populacao = Gerador.gerarPopulacaoAleatoriaIndividuos(configuracao.getTamanhoPopulacao(), configuracao.getTamanhoIndividuo());
        Populacao populacaoAvaliada = avaliador.avaliarPopulacao(populacao, configuracao.getTamanhoNTeste().get(2), configuracao, 0);
        Populacao populacaoOrdenadaFitnessIndividual = ag.ordenaPopulacao(populacaoAvaliada, 0);
        assertEquals(50,populacaoOrdenadaFitnessIndividual.getTamanho());
        Populacao populacaoSelecionadaFitnessIndividual = ag.selecaoPorTorneio(populacaoOrdenadaFitnessIndividual, 0);
        assertEquals(50,populacaoSelecionadaFitnessIndividual.getTamanho());     
        Populacao populacaoCruzada = ag.crossover(populacaoAvaliada);
        Populacao populacaoMutada = ag.mutacao(populacaoCruzada);
        assertEquals(50,populacaoMutada.getTamanho());
        populacaoMutada.exibeIndividuos("populacao mutada");
     }
    
//    @Test
//    public void testMediaPorInteracao() throws IOException {
//        //errado
//        Ag ag = new Ag();
//        Grafico grafico = new Grafico();
//        DefaultCategoryDataset ds = new DefaultCategoryDataset();
//        Avaliador avaliador = new Avaliador();
//            Configuracao config = new Configuracao(100, 0.6, true, 0.9, 5, 0.2, 4);
//            ag.addConfiguracao(config);
//            List<Double> dnaSolucao = Gerador.gerarListaDeValoresReais(config.getTamanhoIndividuo());
//            Cromossomo cromossomoSolucao = new Cromossomo(dnaSolucao, dnaSolucao.size());
//            Individuo individuoSolucao = new Individuo(cromossomoSolucao);        
//            avaliador.setSolucao(dnaSolucao);
//            Populacao melhores = new Populacao();
//            Individuo melhor = new Individuo(Gerador.gerarCromossmoAleatorio(0));
//            melhor.setFitness(0);
//            avaliador.setMelhor(melhor);
//            List<List<Double>> fitnessExecucao = new ArrayList<>();
//            List<Double> fitnessIteracao = new ArrayList<>();
//            for(int w = 0 ; w < 5 ; w++){
//                Populacao populacao = Gerador.gerarPopulacaoAleatoriaIndividuos(config.getTamanhoPopulacao(), config.getTamanhoIndividuo());
//                for(int i = 0 ; i < 100 ; i++){
//                    Populacao populacaoAvaliada = avaliador.avaliarPopulacao(populacao);
//                    Populacao populacaoOrdenada = ag.ordenaPopulacao(populacaoAvaliada);
//                    Populacao  populacaoComEscolhidos = ag.selecaoPorTorneio(populacaoOrdenada);
//                    Populacao populacaoCruzada = ag.crossover(populacaoComEscolhidos) ;
//                    assertEquals(100,populacaoCruzada.getTamanho());
//                    Populacao populacaoMutada = ag.mutacao(populacaoCruzada);
//                    assertEquals(100,populacaoMutada.getTamanho());
//                    Populacao populacaoMutadaAvaliada = avaliador.avaliarPopulacao(populacaoMutada);
//                    avaliador.melhorIndividuoPorInteracao(populacaoMutadaAvaliada) ;  
//                    melhor = avaliador.getMelhor();
//                    fitnessIteracao.add(melhor.getFitness());
//                    populacao = populacaoMutadaAvaliada ;
//                }
//                melhores.exibeIndividuos("Melhores de cada execução");
//                fitnessExecucao.add(fitnessIteracao);
//            }
//            for(int i = 0; i < fitnessIteracao.size(); i++){
//             double soma =0 ;
//             double media = 0;
//             for(int j = 0 ; j < fitnessExecucao.size();j++){
//                    soma += fitnessExecucao.get(j).get(i);
//                    media = soma / 5 ;
//                 }
//                    ds = grafico.configuracaoDataSerGrafico(ds, media, "T" + config.getTamanhoIndividuo() + "N" + config.getTamanhoPopulacao(), i);          
//             }
//             
//         try {
//                grafico.mostraGrafico(grafico.painelGrafico(ds));
//            } catch (IOException ex) {
//                System.out.println(ex.getMessage());
//        }
//   }
   

}
