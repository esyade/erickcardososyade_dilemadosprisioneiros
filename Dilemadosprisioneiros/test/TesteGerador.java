import static org.junit.Assert.*;

import org.junit.Test;

public class TesteGerador {
	
	@Test
	public void testGerarListaValoresBinarios() {
		assertEquals(10,Gerador.gerarListaDeValores(10).size());
	}
        
        @Test
	public void testGeraCromossomoAleatorio() {
                assertEquals(10,Gerador.gerarCromossmoAleatorio(10).getTamanho());
        }
	
	@Test
	public void testGeraIndividuoAleatorio() {
		assertEquals(10,Gerador.gerarIndividuoAleatorio(10).getCromossomo().getTamanho());
	}
	
	@Test
	public void testGeraPopulacaoAleatoria() {
		assertEquals(10,Gerador.gerarPopulacaoAleatoriaIndividuos(10,10).getTamanho());
	}

}
