
import dilemadosprisioneiros.Avaliador;
import dilemadosprisioneiros.Configuracao;
import dilemadosprisioneiros.Cromossomo;
import dilemadosprisioneiros.Individuo;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

public class testeAvaliador {

    @Test
    public void testCalculaFitnessIndividuo() {
        List<Double> tamanhoNTeste = new ArrayList<>(); //nTeste == 1, 10% e 30% ;
        tamanhoNTeste.add(1.0);
        tamanhoNTeste.add(0.10);
        tamanhoNTeste.add(0.30);
        Map<String,Double> valoresDeCasos = new HashMap<>();
        valoresDeCasos.put("melhorCaso",1.0);
        valoresDeCasos.put("casoRazoavel",0.75);
        valoresDeCasos.put("casoIntermediario",0.50);
        valoresDeCasos.put("piorCaso",0.05);
        Configuracao configuracao = new Configuracao(50, 0.8, true, 0.6, 5, 0.01, 50 * 0.02,3,0.05,tamanhoNTeste,valoresDeCasos);
        
        List<Double> dna1 = new ArrayList<>();
        dna1.add(0.21);
        dna1.add(0.12);
        dna1.add(0.11);
        dna1.add(0.52);
        dna1.add(0.01);
        dna1.add(0.50);
        dna1.add(0.90);
        Cromossomo cromossomo1 = new Cromossomo(dna1, dna1.size());
        Individuo individuo1 = new Individuo(cromossomo1);

        List<Double> dna2 = new ArrayList<>();
        dna2.add(0.21);
        dna2.add(0.11);
        dna2.add(0.12);
        dna2.add(0.51);
        dna2.add(0.74);
        dna2.add(0.91);
        dna2.add(0.01);
        
        Cromossomo cromossomo2 = new Cromossomo(dna2, dna2.size());
        Individuo individuo2 = new Individuo(cromossomo2);
        
        Avaliador avaliador = new Avaliador();
        avaliador.addConfiguracao(configuracao);
        assertEquals(2, avaliador.calculaFitnessIndividuo(individuo1,individuo2));
    }
    
    
    

}
