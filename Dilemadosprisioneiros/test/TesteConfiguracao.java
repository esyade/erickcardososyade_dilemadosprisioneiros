import static org.junit.Assert.*;

import org.junit.Test;

public class TesteConfiguracao {
    /**
     * Configuracao configuracao = new Configuracao();
        configuracao.setTamanhoPopulacao(50);
        configuracao.setTamanhoIndividuo(30);
        configuracao.setProbabilidadeCruzamento(0.6);
        configuracao.setProbabilidadeMutacao(0.3);
        configuracao.setTamanhoSelecao(5);
        configuracao.setVariancia(0.08);
        configuracao.setTaxaBonus(0.05);
        configuracao.setCondicaoBonus(4);
        configuracao.addNTeste(1.0);
        configuracao.addNTeste(0.10 * configuracao.getTamanhoPopulacao());
        configuracao.addNTeste(0.50 * configuracao.getTamanhoPopulacao());
        configuracao.addValorDeCaso("dc_individuo",1.0);
        configuracao.addValorDeCaso("cc_individuo",0.90);
        configuracao.addValorDeCaso("dd_individuo",0.50);
        configuracao.addValorDeCaso("cd_individuo",0.01);
        configuracao.addValorDeCaso("cc_grupo",1.0);
        configuracao.addValorDeCaso("cd_grupo",0.50);
        configuracao.addValorDeCaso("dc_grupo",0.50);
        configuracao.addValorDeCaso("dd_grupo",0.01);
        configuracao.setTipoAvaliacao("indvidual");
        configuracao.setTaxaClassificaoCD(0.5);
     */
	
	@Test
	public void testTamanhoPopulacao() {
            Configuracao config = new Configuracao();
            config.setTamanhoPopulacao(100);
            assertEquals(100, config.getTamanhoPopulacao());
	}
        
        @Test
	public void testTamanhoIndividuo() {
            Configuracao config = new Configuracao();
            config.setTamanhoIndividuo(30);
            assertEquals(30, config.getTamanhoIndividuo());
	}
	
	@Test
	public void testProbabilidadeCruzamento() {
            Configuracao config = new Configuracao();
            config.setProbabilidadeCruzamento(0.9);
            assertEquals(0.9, config.getProbabilidadeCruzamento(),0);
	}
	
	@Test
	public void testProbabilidadeMutacao() {
            Configuracao config = new Configuracao();
            config.setProbabilidadeMutacao(0.9);
            assertEquals(0.9, config.getProbabilidadeMutacao(),0);
	}
	
        @Test
	public void testTaxaBonus() {
            Configuracao config = new Configuracao();
            config.setTaxaBonus(0.9);
            assertEquals(0.9, config.getTaxaBonus(),0);
	}
        
        @Test
	public void testCondicaoBonus() {
            Configuracao config = new Configuracao();
            config.setCondicaoBonus(9);
            assertEquals(9, config.getCondicaoBonus(),0);
	}
        
        @Test
	public void testVariancia() {
            Configuracao config = new Configuracao();
            config.setVariancia(0.9);
            assertEquals(0.9, config.getVariancia(),0);
	}
        
        @Test
	public void testNTeste() {
            Configuracao config = new Configuracao();
            config.addNTeste(1.0);
            config.addNTeste(2.0);
            config.addNTeste(3.0);
            assertEquals(1.0, config.getTamanhoNTeste().get(0),0);
            assertEquals(2.0, config.getTamanhoNTeste().get(1),0);
            assertEquals(3.0, config.getTamanhoNTeste().get(2),0);
	}
        
        @Test
        public void testTaxaClassificaoCD(){
            Configuracao config = new Configuracao();
            config.setTaxaClassificaoCD(0.5);
            assertEquals(0.5, config.getTaxaClassificaoCD(),0);
        }
       
	

}
