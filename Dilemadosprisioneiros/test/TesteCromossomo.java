
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class TesteCromossomo {

    @Test
    public void testSetDna() {
        List<Double> dna = new ArrayList<>();
        dna.add(0.11);
        dna.add(0.2);
        dna.add(0.16);
        dna.add(0.65);
        dna.add(0.56);
        Cromossomo cromossomo = new Cromossomo(dna, dna.size());
        assertEquals(5, cromossomo.getDna().size());
    }

    @Test
    public void testAddGene() {
        List<Double> dna = new ArrayList<>();
        int locusGenico = 1;
        Cromossomo cromossomo = new Cromossomo(dna, dna.size());
        dna.add(0.16);
        dna.add(0.65);
        dna.add(0.56);
        assertEquals(0.65, cromossomo.getGene(locusGenico),0);
    }

}
